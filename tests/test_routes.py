import pytest
from flask import url_for
from app import create_app
from unittest.mock import patch
from app.models import User


@pytest.fixture
def app():
    app = create_app(
        {"TESTING": True, "MONGO_URI": "mongodb://mockhost:27017/test_database"}
    )
    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def mock_user_class(mocker):
    mocker.patch("app.routes.User", autospec=True)


def test_home_page(client):
    response = client.get("/")
    assert response.status_code == 200
    assert b"Welcome to the User Management App!" in response.data


def test_register(mock_user_class, client):
    response = client.post(
        "/register",
        data={"username": "testuser", "password": "testpass"},
        follow_redirects=True,
    )
    assert (
        b"Login" in response.data
    )  # Assuming the redirect to login happens after registration


def test_login(mock_user_class, client):
    client.post("/register", data={"username": "testuser", "password": "testpass"})
    response = client.post(
        "/login", data={"username": "testuser"}, follow_redirects=True
    )
    assert b"Login Successful!" in response.data
