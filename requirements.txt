Flask==2.1.0
pymongo==3.12.0
pytest==6.2.5
Werkzeug==2.0.0
pytest-mock==3.6.1
twine==5.0.0
